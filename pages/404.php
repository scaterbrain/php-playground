<?php
?>
<!DOCTYPE HTML>
<html>
  <head>
    <title>404</title>
    <link rel="stylesheet" href="style.css"/>
    <link rel="icon" type="image/x-icon" href="favicon.ico">
  </head>
  <body>
    <p>Pages not found.</p>
  </body>
</html>
