<?php

namespace app;

/**
 * Description of Response
 *
 * @author winston
 */
final class Response
{

    private string $pages;
    private array $routes = array(
        '/' => 'home.php',
        '/404' => '404.php',
    );

    public function __construct(string $url)
    {
        $this->pages = isset($this->routes[$url]) ? $this->routes[$url] : '404.php';
    }

    public function getResponse(): string
    {
        return $this->pages;
    }

}
