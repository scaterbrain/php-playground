<?php

namespace app;

require '../app/Request.php';
require '../app/Response.php';

/**
 * Description of App
 *
 * @author winston
 */
final class App
{

    private Request $request;

    public function __construct()
    {
        error_log("App created");
        $this->request = new Request();
    }

    public function run()
    {
        error_log("App run");
        $response = new Response($this->request->getUrl());
        error_log($response->getResponse());
        require_once '../pages/' . $response->getResponse();
    }

}
