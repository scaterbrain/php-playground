<?php

namespace app;

/**
 * Description of Request
 *
 * @author winston
 */
final class Request
{

    private array $request = [];

    public function __construct()
    {
        $this->request = (array) filter_input_array(INPUT_SERVER);
    }

    public function getUrl(): string
    {
        return $this->request['REQUEST_URI'];
    }

    public function getMethodType(): string
    {
        return $this->request['REQUEST_METHOD'];
    }
    
    public function getRequest()
    {
        return $this->request;
    }
    
    public function printReqest() : void 
    {
        foreach ($this->request as $key => $value) {
            error_log($key . " " . $value);
        }
    }

}
